//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 扇贝刷题.rc 使用
//
#define IDD_MY_DIALOG                   102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_QUESTION               1000
#define IDC_RADIO_A                     1001
#define IDC_RADIO_B                     1002
#define IDC_RADIO_C                     1003
#define IDC_RADIO_D                     1004
#define IDC_CHECK_NOTSURE               1005
#define IDC_BUTTON_SUMMIT               1006
#define IDC_BUTTON1                     1007
#define IDC_BUTTON_NEXT                 1007
#define IDC_RADIO_YES                   1009
#define IDC_RADIO_NO                    1010
#define IDC_BUTTON_START                1011
#define IDC_EDIT_STARTINFO              1012
#define IDC_STATIC_QUESTION             1013

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
