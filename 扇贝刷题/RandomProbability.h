#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <string>
#include "afxwin.h"

using namespace std;

class RandomProbability{
public:
	RandomProbability(int,string);
	int GenerateRandomIndex();
	void possibility_adjust(int aim,int result_right,int result_sure);
private:
	int num_of_ques;	//题目总数
	int* probability;	//概率数组
	int sum_of_prob;	//概率总和（未归一化）
	int sum(int prob_index);
	string filename_possi;
	ifstream fin;
	ofstream fout;
};
