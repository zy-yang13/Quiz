
// 扇贝刷题Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "扇贝刷题.h"
#include "扇贝刷题Dlg.h"
#include "afxdialogex.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C扇贝刷题Dlg 对话框



C扇贝刷题Dlg::C扇贝刷题Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(C扇贝刷题Dlg::IDD, pParent)
	, m_string_startinfo(_T(""))
	, m_string_question(_T(""))
	, m_int_abcd(0)
	, m_int_yesno(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void C扇贝刷题Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_QUESTION, m_edit_question);
	DDX_Control(pDX, IDC_RADIO_A, m_radio_A);
	DDX_Control(pDX, IDC_RADIO_YES, m_radio_YES);
	DDX_Control(pDX, IDC_CHECK_NOTSURE, m_check_notsure);
	DDX_Control(pDX, IDC_EDIT_STARTINFO, m_edit_startinfo);
	DDX_Control(pDX, IDC_BUTTON_START, m_button_start);
	DDX_Control(pDX, IDC_BUTTON_SUMMIT, m_button_summit);
	DDX_Text(pDX, IDC_EDIT_STARTINFO, m_string_startinfo);
	DDX_Text(pDX, IDC_EDIT_QUESTION, m_string_question);
	DDX_Radio(pDX, IDC_RADIO_A, m_int_abcd);
	DDX_Radio(pDX, IDC_RADIO_YES, m_int_yesno);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_button_next);
}

BEGIN_MESSAGE_MAP(C扇贝刷题Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_START, &C扇贝刷题Dlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_SUMMIT, &C扇贝刷题Dlg::OnBnClickedButtonSummit)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &C扇贝刷题Dlg::OnBnClickedButtonNext)
END_MESSAGE_MAP()


// C扇贝刷题Dlg 消息处理程序

BOOL C扇贝刷题Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	filename="problem.txt";
	filename_possi="problem_possi.txt";
	fin_problem.open(filename.c_str());
	fin_problem>>num_of_ques;
	prandom=new RandomProbability(num_of_ques,filename_possi);
	ques_index=prandom->GenerateRandomIndex();
	pquest=new Question(filename,ques_index);
	startinfo="当前题目包："+filename;
	m_string_startinfo=startinfo.c_str();
	UpdateData(FALSE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void C扇贝刷题Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR C扇贝刷题Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void C扇贝刷题Dlg::OnBnClickedButtonStart()
{
	// TODO: 在此添加控件通知处理程序代码

	m_edit_startinfo.ShowWindow(SW_HIDE);
	m_button_start.ShowWindow(SW_HIDE);
	m_edit_question.ShowWindow(SW_SHOW);
	GetDlgItem(IDC_STATIC_QUESTION)->ShowWindow(SW_SHOW);
	if(pquest->num_of_options!=0)
	{
		GetDlgItem(IDC_RADIO_A)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_B)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_C)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_D)->ShowWindow(SW_SHOW);
	}
	else
	{
		GetDlgItem(IDC_RADIO_YES)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_NO)->ShowWindow(SW_SHOW);
	}
	m_check_notsure.ShowWindow(SW_SHOW);
	m_button_summit.ShowWindow(SW_SHOW);
	QuesText=pquest->GetText();
	m_string_question=QuesText.c_str();
	UpdateData(FALSE);
}


void C扇贝刷题Dlg::OnBnClickedButtonSummit()
{
	// TODO: 在此添加控件通知处理程序代码
	if(pquest->num_of_options!=0)
	{
		if(GetCheckedRadioButton(IDC_RADIO_A,IDC_RADIO_D)==IDC_RADIO_A) my_ans[0]='A';
		if(GetCheckedRadioButton(IDC_RADIO_B,IDC_RADIO_D)==IDC_RADIO_B) my_ans[0]='B';
		if(GetCheckedRadioButton(IDC_RADIO_C,IDC_RADIO_D)==IDC_RADIO_C) my_ans[0]='C';
		if(GetCheckedRadioButton(IDC_RADIO_D,IDC_RADIO_D)==IDC_RADIO_D) my_ans[0]='D';
	}
	else
	{
		if(m_radio_YES.GetCheck()==1) my_ans[0]='Y';
		else my_ans[0]='N';
	}
	if(m_check_notsure.GetCheck()==1)
	{
		my_ans[1]='?';
		my_ans[2]='\0';
	}
	else my_ans[1]='\0';
	pquest->InputAnswer(my_ans);
	if(pquest->result_right==1) QuesText=QuesText+"\r\n回答正确！";
	else QuesText=QuesText+"\r\n回答错误！";
	prandom->possibility_adjust(pquest->ques_index,pquest->result_right,pquest->result_sure);
	m_string_question=QuesText.c_str();
	m_button_next.ShowWindow(SW_SHOW);
	m_check_notsure.ShowWindow(SW_HIDE);
	m_button_summit.ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RADIO_A)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RADIO_B)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RADIO_C)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RADIO_D)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RADIO_YES)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_RADIO_NO)->ShowWindow(SW_HIDE);
	
	UpdateData(FALSE);

}


void C扇贝刷题Dlg::OnBnClickedButtonNext()
{
	// TODO: 在此添加控件通知处理程序代码
	
	pquest->ChangeQuestion(filename,prandom->GenerateRandomIndex());
	m_button_next.ShowWindow(SW_HIDE);
	if(pquest->num_of_options!=0)
	{
		GetDlgItem(IDC_RADIO_A)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_B)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_C)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_D)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_YES)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO_NO)->ShowWindow(SW_HIDE);
	}
	else
	{
		GetDlgItem(IDC_RADIO_A)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO_B)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO_C)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO_D)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_RADIO_YES)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_RADIO_NO)->ShowWindow(SW_SHOW);
	}
	m_check_notsure.ShowWindow(SW_SHOW);
	m_button_summit.ShowWindow(SW_SHOW);
	QuesText=pquest->GetText();
	m_string_question=QuesText.c_str();
	UpdateData(FALSE);
}


