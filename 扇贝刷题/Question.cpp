#include "stdafx.h"
#include "Question.h"

using namespace std;

Question::Question(string filename, int q)
{
	ques_index=q;
	string* options = NULL;	
	ReadQuestionFromText(filename,ques_index);
}    //构造函数
void Question::InputAnswer(char* ans){
	if(ans[1] == '?'){
		if(ans[0] == right_ans){
			result_right=1;
			result_sure=0;
		}
		else{
			result_right=0;
			result_sure=0;
		}	
	}
	else if(ans[1] == '\0'){
		if(ans[0] == right_ans)
		{
			result_right=1;
			result_sure=1;
		}
		else
		{
			result_right=0;
			result_sure=1;
		}
	}
	else{
		cout << "Illegal input! Please try again." << endl;
		exit(-1);
	}
}	//接受用户输入答案并进行判断，与正确答案一致则输出R，否则输出W
void Question::ChangeQuestion(string filename, int n){
	ques_index=n;
	ReadQuestionFromText(filename,ques_index);
}	//修改题目
string Question::GetText(){
	string text="";
	text=text+ques+"\r\n";
	if(num_of_options != 0){
		char option_index = 'A'; 
		for(int i = 0; i < num_of_options; i++){
			text=text+option_index+'.'+options[i]+"\r\n";
			option_index ++;
		}
	}
	else{
		text=text+"(Y/N)"+"\r\n";
	}
	return text;
}	//显示题目及题目选项
void Question::ReadQuestionFromText(string filename, int ques_index){
	char chr;
	int index;
	int num_of_ques;
	string option="";
	ifstream fin(filename.c_str());
	if(!fin){
		cout << "Cannot open file \"" << filename << "\"" << endl;
		exit(-1);
	}
	fin >> num_of_ques;	//获得题目数量
	if(ques_index > num_of_ques){
		cout << "Illegal question index!" << endl;
		exit(-2);
	}	//判断题号是否大于题目数量
	while(true){
		fin>>chr;
		if(chr=='#'){
			fin>>index;
			if(index==ques_index) break;
		}//寻找相应的题号
	}
	fin>>num_of_options;	//获得题目选项数量
	ques="";
	while(ques=="") getline(fin,ques);
	if(num_of_options != 0){
		options = new string[num_of_options];
		for(int j = 0; j < num_of_options; j++){
			options[j]="";
			while(options[j]=="") getline(fin,options[j]);
		}
	}
	else{
		options = NULL;
	}//输出题目选项
	fin>>right_ans;	//答案
}	//从题目包中读取题目