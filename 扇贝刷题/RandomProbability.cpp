#include "stdafx.h"
#include "RandomProbability.h"

RandomProbability::RandomProbability(int n,string str)
{
	num_of_ques=n;
	filename_possi=str;
	fin.open(filename_possi.c_str());
	probability = new int[num_of_ques];
};
int RandomProbability::GenerateRandomIndex(){
	int random_num;
	int index = 1;
	srand(unsigned(time(0)));
	sum_of_prob = 0;
	fin.clear();
	for(int i = 0; i < num_of_ques; i++){
		fin >> probability[i];
		sum_of_prob += probability[i];
	}
	random_num =rand() % (sum_of_prob + 1);		//生成一个从0~sum_of_prob之间的随机数
	//cout << "random_num = " << random_num << endl;
	while(sum(index) < random_num){
		index ++;
	}//将random_num按概率映射到1~num_of_ques上
	/*if(index > num_of_ques){
		cout << "Index out of range!" << endl;
		exit(-1);
	}*/
	return index;
}//生成随机的题目序号
int RandomProbability::sum(int prob_index)
{
	int result=0;
	for(int i=0; i<prob_index; i++){
			result += probability[i];
	}
	return result;
}	//计算前prob_index个数组的概率之和

void RandomProbability::possibility_adjust(int aim,int result_right,int result_sure)
{
	if(result_sure==1&&result_right==1)
	{
		probability[aim-1]=0;
	}
	else if(result_sure==0&&result_right==1)
	{
		if(probability[aim-1]>1) probability[aim-1]-=1;
	}
	else if(result_sure==1&&result_right==0)
	{
		if(probability[aim-1]<10) probability[aim-1]+=2;
	}
	else if(result_sure==0&&result_right==0)
	{
		if(probability[aim-1]<10) probability[aim-1]+=1;
	}
	fin.close();
	fout.open(filename_possi);
	for(int i = 0; i < num_of_ques; i++) fout<<probability[i]<<endl;
	fout.close();
	fin.open(filename_possi.c_str());

}