#pragma once
#include "afxwin.h"
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

class Question
{
public:
	Question(string,int);
	void InputAnswer(char* ans);
	void ChangeQuestion(string filename, int n);
	string GetText();
	int num_of_options;	//选项个数
	int ques_index;	//选项个数
	int result_right,result_sure;
private:
	string ques;	//题目描述
	string* options;	//题目选项
	char right_ans;	//题目正确答案
	void ReadQuestionFromText(string filename, int ques_index);
};

