
// 扇贝刷题Dlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include "RandomProbability.h"
#include "Question.h"

using namespace std;

// C扇贝刷题Dlg 对话框
class C扇贝刷题Dlg : public CDialogEx
{
// 构造
public:
	C扇贝刷题Dlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_MY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit m_edit_question;
	CButton m_radio_A;
	CButton m_radio_YES;
	CButton m_check_notsure;
	CEdit m_edit_startinfo;
	afx_msg void OnBnClickedButtonStart();
	CButton m_button_start;
	CButton m_button_summit;
	CString m_string_startinfo;
	char my_ans[3];
	int num_of_ques;
	int ques_index;
	string startinfo;
	string filename;
	string filename_possi;
	ifstream fin_problem;
	RandomProbability *prandom;
	Question *pquest;
	CString m_string_question;
	afx_msg void OnBnClickedButtonSummit();
	int m_int_abcd;
	int m_int_yesno;
	string QuesText;
	CButton m_button_next;
	afx_msg void OnBnClickedButtonNext();
};
